#!/usr/bin/env python3

from ranga.app import App

def main():
    App().run()

if __name__ == '__main__':
    main()
