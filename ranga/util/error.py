"""
application custom errors
"""
class FileParsingError(Exception):
    pass

class SchemaValidationError(Exception):
    pass
