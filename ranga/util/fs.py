"""
file system utilities
"""

from json import JSONDecodeError, load as load_json
from yaml import YAMLError, safe_load as load_yaml
from os.path import dirname, realpath, join as join_path

from .error import FileParsingError

def dirpath(filepath: str) -> str:
    "return parent directory path of a given file path"
    return dirname(realpath(filepath))

def load_dict(path: str) -> dict:
    "load JSON or YAML file"
    result = None
    with open(path) as file:
        try: result = load_json(file)
        except JSONDecodeError: None
    if result is None:
        with open(path) as file:
            try: result = load_yaml(file)
            except YAMLError: None
    if result is None:
        raise FileParsingError(f"invalid dict structure in {path}")
    return result
