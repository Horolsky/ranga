import os
from os.path import dirname, realpath
from pathlib import Path
from sqlite3.dbapi2 import Cursor
from typing import Any, Dict, Generator
import peewee as pw
from tabulate import tabulate
from magic import from_file

import hashlib



def file_sha1(path: str) -> str:
    BUF_SIZE = 65536
    sha1 = hashlib.sha1()

    with open(path, 'rb') as f:
        while True:
            data = f.read(BUF_SIZE)
            if not data:
                break
            sha1.update(data)
    return sha1.hexdigest()


def get_basic_metadata(path: str) -> Dict:
    data: os.stat_result = os.stat(path)
    KEYS = ('st_atime_ns','st_ctime_ns','st_mtime_ns', 'st_size', 'st_nlink')
    data = {k[3:]: getattr(data, k) for k in KEYS}
    data['SHA-1'] = None if not os.path.isfile(path) else file_sha1(path)
    return data

SCRIPT_DIR = dirname(realpath(__file__))
DB_SCHEMA = f'{SCRIPT_DIR}/schema.sql'

RANGA_APPDIR = os.getenv('RANGA_APPDIR', f"{Path.home()}/.ranga")
DB_PATH = f'{RANGA_APPDIR}/index.db' #TODO move this to yml config

DB = pw.SqliteDatabase(DB_PATH)
CURSOR: Cursor = DB.cursor()

def initialize_model():
    with open(DB_SCHEMA, 'r') as sql_file:
            CURSOR.executescript(sql_file.read())
            DB.commit()

class FileMode:
    "file mode flags"
    DIR = 0b01 # isdir
    LNK = 0b10 # islink

    @staticmethod
    def get(path: str) -> int:
        dr = os.path.isdir(path)
        ln = os.path.islink(path) << 1
        return dr | ln

    @staticmethod
    def is_dir(mode: int) -> bool:
        return bool(mode & FileMode.DIR)

    @staticmethod
    def is_file(mode: int) -> bool:
        return not FileMode.is_dir(mode)

    @staticmethod
    def is_link(mode: int) -> bool:
        return bool(mode & FileMode.LNK)


class BaseModel(pw.Model):

    # @classmethod
    @classmethod
    def stringify(TABLE, mode: str, header: bool):
        """
        return formatted stringified table
        """
        q = TABLE.select()
        return tabulate(q.dicts(), headers='keys' if header else '', tablefmt=mode)

    class Meta:
        database = DB

class BaseModelWithPK(BaseModel):
    id = pw.AutoField(column_name='id', null=True)

class Node(BaseModelWithPK):
    parent = pw.ForeignKeyField(column_name='parent', field='id', model='self', null=True)
    path = pw.CharField(unique=True)

    class Meta:
        table_name = 'Node'

    @staticmethod
    def synchronize(path: str, parent = 'default') -> 'Node':
        "update or create node"

        if parent == 'default':
            parent = Node.get_or_none(Node.path == os.path.dirname(path))

        node: Node = None
        node, _ = Node.get_or_create(path=path, parent=parent)
        # node.save()

        statdata: os.stat_result = os.stat(path)
        mode = FileMode.get(path)
        node.update_category('atime', statdata.st_atime_ns)
        node.update_category('mtime', statdata.st_mtime_ns)
        node.update_category('size', statdata.st_size)
        node.update_category('sha1', None if not os.path.isfile(path) else file_sha1(path))
        node.update_category('mode', mode)

        filename = os.path.basename(path)
        node.update_category('filename', filename)

        if FileMode.is_file(mode):
            extension = None if '.' not in filename else filename[filename.rindex('.')+1:]
            node.update_category('extension', extension)

            filemagic = from_file(path)
            node.update_category('filemagic', filemagic)

        return node

    @staticmethod
    def roots() -> Generator['Node', None, None]:
        return Node.select().where(Node.parent == None)

    def children(self) -> Generator['Node', None, None]:
        return Node.select().where(Node.parent == self)

    @property
    def filename(self):
        return os.path.basename(self.path)

    # @property
    # def isdir(self):
        # return FileMode.is_dir(self.mode)
    # @property
    # def isfile(self):
        # return FileMode.is_file(self.mode)
    # @property
    # def islink(self):
        # return FileMode.is_link(self.mode)

    def update_category(self, label: str, value: Any):
        'create or update Category arc and return it'
        k: Key = None
        a: Attribute = None
        c: Category = None

        k, _ = Key.get_or_create(label=label)
        a, _ = Attribute.get_or_create(key=k,value=value)
        c, _ = Category.get_or_create(attribute=a,node=self)

        return c

    def get_attributes(self, label: str) -> Generator['Attribute', None, None]:
        k = Key.get_or_none(Key.label==label)
        return (md.attribute for md in MetaData.select().where(MetaData.key == k))



class Key(BaseModelWithPK):
    descr = pw.CharField(null=True)
    label = pw.CharField(null=True, unique=True)

    class Meta:
        table_name = 'Key'

class Attribute(BaseModelWithPK):
    key = pw.ForeignKeyField(column_name='key', field='id', model=Key)
    value = pw.AnyField(index=True, null=True)

    class Meta:
        table_name = 'Attribute'

class Category(BaseModel):
    attribute = pw.ForeignKeyField(column_name='attribute', field='id', model=Attribute)
    node = pw.ForeignKeyField(column_name='node', field='id', model=Node)

    class Meta:
        table_name = 'Category'
        primary_key = False

class Variable(BaseModel):
    key = pw.CharField(null=True, unique=True)
    value = pw.AnyField(null=True)

    class Meta:
        table_name = 'Variable'
        primary_key = False

class AttributeView(BaseModel):
    key = pw.ForeignKeyField(column_name='key', field='id', model=Key)
    attribute = pw.ForeignKeyField(column_name='attribute', field='id', model=Attribute)
    label = pw.CharField()
    descr = pw.CharField()
    value = pw.AnyField()

    class Meta:
        table_name = 'AttributeView'
        primary_key = False

class MetaData(BaseModel):
    node = pw.ForeignKeyField(column_name='node', field='id', model=Node)
    path = pw.CharField()
    parent = pw.ForeignKeyField(column_name='parent', field='id', model=Node)
    # mtime = pw.FloatField()
    # mode = pw.IntegerField()
    # filetype = pw.CharField(column_name='filetype')
    key = pw.ForeignKeyField(column_name='key', field='id', model=Key)
    attribute = pw.ForeignKeyField(column_name='attribute', field='id', model=Attribute)
    label = pw.CharField()
    descr = pw.CharField()
    value = pw.AnyField()

    class Meta:
        table_name = 'MetaData'
        primary_key = False


TABLES: Dict[str, BaseModel] = {
    'Node': Node,
    'Key': Key,
    'Attribute': Attribute,
    'Category': Category,
    'AttributeView': AttributeView,
    'MetaData': MetaData,
}
