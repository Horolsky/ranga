PRAGMA foreign_keys = ON;

-- DATA TABLES

/* node registry */
CREATE TABLE IF NOT EXISTS [Node] (
    [id] INTEGER PRIMARY KEY,
    [path] VARCHAR(4096) NOT NULL UNIQUE,
    [parent] INTEGER, -- NULL if root entry
    FOREIGN KEY( [parent] )
        REFERENCES [Node]( [id] )
        ON UPDATE CASCADE
        ON DELETE CASCADE
);

/* metadata key */
CREATE TABLE IF NOT EXISTS [Key] (
    [id] INTEGER PRIMARY KEY,
    [label] VARCHAR(256) UNIQUE,
    [descr] VARCHAR(4096)
);

/* metadata attribute */
CREATE TABLE IF NOT EXISTS [Attribute] (
    [id] INTEGER PRIMARY KEY,
    [key] INTEGER NOT NULL,
    [value] ANY, -- weak typing
    FOREIGN KEY( [key] )
        REFERENCES [Key]( [id] )
        ON DELETE CASCADE
);

/* metadata map */
CREATE TABLE IF NOT EXISTS [Category] (
    [node] INTEGER NOT NULL,
    [attribute] INTEGER NOT NULL,
    FOREIGN KEY( [node] )
        REFERENCES [Node]( [id] )
        ON DELETE CASCADE,
    FOREIGN KEY( [attribute] )
        REFERENCES [Attribute]( [id] )
        ON DELETE CASCADE
);

-- DATA INDICI

CREATE INDEX IF NOT EXISTS [idx_node_paths]
ON [Node] ( [path] );

CREATE INDEX IF NOT EXISTS [idx_node_parent]
ON [Node] ( [parent] );

CREATE UNIQUE INDEX IF NOT EXISTS [idx_key_label]
ON [Key] ( [label] );

CREATE INDEX IF NOT EXISTS [idx_attr_val]
ON [Attribute] ( [value] );
-- TODO test performance for weak typing

CREATE INDEX IF NOT EXISTS [idx_attr_key]
ON [Attribute] ( [key] );

CREATE INDEX IF NOT EXISTS [idx_cat_node]
ON [Category] ( [node] );

CREATE INDEX IF NOT EXISTS [idx_cat_attr]
ON [Category] ( [attribute] );


-- DATA VIEWS

/* metadata + label info */
CREATE VIEW IF NOT EXISTS [AttributeView] AS
SELECT
    [Key].[id] as [key],
    [Attribute].[id] as [attribute],
    [Key].[label],
    [Key].[descr],
    [Attribute].[value]
FROM
    [Key]
    JOIN [Attribute] ON [Attribute].[key] = [Key].[id];

/* file + metadata view */
CREATE VIEW IF NOT EXISTS [MetaData] AS
SELECT
    [Node].[id] as [node],
    [Node].[path],
    [Node].[parent],

    [AttributeView].[key],
    [AttributeView].[attribute],
    [AttributeView].[label],
    [AttributeView].[descr],
    [AttributeView].[value]

FROM [AttributeView]
    JOIN [Category] ON [Category].[attribute] = [AttributeView].[attribute]
    JOIN [Node] ON [Category].[node] = [Node].[id];

-- UTILITY

CREATE TABLE IF NOT EXISTS [Variable] (
    [key] VARCHAR(16) UNIQUE,
    [value] ANY
);

CREATE TRIGGER IF NOT EXISTS [UpdateVariable]
    BEFORE INSERT ON [Variable]
BEGIN
    DELETE FROM [Variable] WHERE [key] = NEW.[key];
END;

-- INSERTION FILTERS

CREATE TRIGGER IF NOT EXISTS [UpdateAttribute]
BEFORE INSERT ON [Attribute]
    WHEN EXISTS (
        SELECT [key] FROM [Attribute]
        WHERE [key] = NEW.[key]
        AND [value] = NEW.[value])
BEGIN
    SELECT RAISE(IGNORE);
END;

CREATE TRIGGER IF NOT EXISTS [UpdateCategory]
BEFORE INSERT ON [Category]
    WHEN EXISTS (
        SELECT [node] FROM [Category]
        WHERE [node] = NEW.[node]
        AND [attribute] = NEW.[attribute])
BEGIN
    SELECT RAISE(IGNORE);
END;

-- SETTERS


CREATE TRIGGER IF NOT EXISTS [InsertNode]
AFTER INSERT ON [Node]
BEGIN
    UPDATE Node
    SET [parent] = CASE
        WHEN TYPEOF(NEW.[parent]) = 'text'
            THEN (SELECT [id] FROM [Node]
                WHERE [path] = NEW.[parent] LIMIT 1)
        ELSE NEW.[parent]
    END
    WHERE [id] = NEW.[id];

    INSERT INTO Variable ([key], [value]) VALUES
    (
        'last_inserted_node',
        NEW.[id]
    );
END;

CREATE TRIGGER IF NOT EXISTS [InsertMetaData]
INSTEAD OF INSERT ON [MetaData]
BEGIN

    INSERT OR IGNORE INTO [Key] (label) VALUES
    (
        NEW.[label]
    );

    INSERT OR IGNORE INTO [Attribute] (key, value) VALUES
    (
        (SELECT [id] FROM [Key] WHERE [label] = NEW.[label] LIMIT 1),
        NEW.[value]
    );

    INSERT INTO [Category] (node, attribute) VALUES
    (
        CASE
            WHEN NEW.[path] IS NOT NULL THEN
                (SELECT [id] FROM [Node] WHERE [path] = NEW.[path] LIMIT 1)
            ELSE NEW.[node]
        END,
        (SELECT [attribute] FROM [AttributeView] WHERE [value] = NEW.[value] AND [label] = NEW.[label] LIMIT 1)
    );

END;

