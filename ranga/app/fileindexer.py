from argparse import ArgumentError
from enum import IntFlag
import os
import os.path
from typing import Dict, Iterable, List, Set
from ranga.db.model import *

class FileIndexer:

    @staticmethod
    def walk_dir( root: str) -> List[Node]:

        if not os.path.isdir(root):
            raise FileNotFoundError(f'{root} is not a directory')

        root = os.path.abspath(root)


        for directory, dirs, files in os.walk(root, topdown=True):
            paths = (*dirs, *files)
            node = Node.synchronize(directory)

            for child in node.children():
                if child.filename not in paths:
                    child.delete_instance()

            for path in paths:
                Node.synchronize(os.path.join(directory, path), node)


    def __init__(self) -> None:
        initialize_model()

    def update(self, nodes: Set[str]) -> None:
        nodes = set(nodes)
        if not nodes: return
        for node in nodes:
            FileIndexer.walk_dir(node)

    def execute(self, command, args) -> str:

        args = set(args)
        if 'all' in args and command != 'update':
            raise ArgumentError(f'--{command} cannot be used with `all` parameter')

        if 'all' in args:
            args ^= {'all'}
            args |= { file.path for file in Node.roots() }
        args = { os.path.abspath(path) for path in args }

        print("execute: ", command, args)

        if command in ('update', 'add'):
            self.update(args)
            response = "fs updated"

        elif command == 'remove':
            for node in Node.select().where(Node.path << args):
                node.delete_instance()
            response = "files removed"

        return response
