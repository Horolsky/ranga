from argparse import Namespace
import sys
from typing import List, Union
from ranga.cli import ARGPARSER
from ranga.app.cmd_exec import execute_command
from ranga.db.model import initialize_model


class App:
    """
    metagen application
    """

    def __init__(self, args: Union[List[str], Namespace, None] = None):
        """
        args : List[str] | argparse.Namespace | None
            The command line arguments.
            If None, args are taken from the sys.argv (default)
        """
        if args is None:
            args = ARGPARSER.parse_args(sys.argv[1:])
        elif isinstance(args, List):
            args = ARGPARSER.parse_args(args)
        elif not isinstance(args, Namespace):
            raise RuntimeError("invalid application arguments type")
        self._args = args
        initialize_model()

    def run(self):
        """
        execute application
        """
        if self._args.command is None:
            ARGPARSER.print_help()
            sys.exit(0)
        else:
            execute_command(vars(self._args))
            # raise NotImplementedError()
