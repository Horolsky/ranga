from typing import Iterable
from functools import reduce

from ranga.app.fileindexer import FileIndexer
from ranga.db.model import TABLES, Node, MetaData
from tabulate import tabulate, tabulate_formats
def DbManager():
    pass

FILE_ATTRIBUTES = tuple(Node._meta.columns.keys())
METADATA_ATTRIBUTES = tuple(MetaData._meta.columns.keys())

def search_by_keyword(keywords: Iterable[str], categories: Iterable[str]) -> str:
    """
    query files that meets the search conditions
    """
    if not categories:
        return MetaData.select(
            MetaData.path, MetaData.label, MetaData.value
            ).where(reduce(lambda a, b: (MetaData.path ** a) | (MetaData.path ** b), keywords))

    else:
        return MetaData.select(
            MetaData.path, MetaData.label, MetaData.value
            ).where(reduce(lambda a, b: (MetaData.value ** a) | (MetaData.value ** b), keywords) & (MetaData.label << categories))


def exec_search(**kwargs):

    keywords = kwargs['keywords']
    categories = kwargs['category']
    mode = kwargs['mode']
    header = not kwargs['headless']
    output = search_by_keyword(keywords, categories)

    s = tabulate(output.dicts(), headers='keys' if header else '', tablefmt=mode)

    print(s)

def exec_show(**kwargs):
    table = kwargs['table']
    mode = kwargs['mode']
    header = not kwargs['headless']
    if table in TABLES:
        TABLE = TABLES[table]
        output = TABLE.stringify(mode, header)
        print(output)
    elif table in METADATA_ATTRIBUTES:
        query = MetaData.select(
                MetaData.path,
                getattr(MetaData, table)
            )

        print(tabulate(query.dicts(), headers='keys' if header else '', tablefmt=mode))

    elif table in FILE_ATTRIBUTES:
        query = Node.select(
            Node.path,
            getattr(Node, table)
        )
        print(tabulate(query.dicts(), headers='keys' if header else '', tablefmt=mode))
    else:
        query = MetaData.select(
                MetaData.path,
                MetaData.value.alias(table)
            ).where(MetaData.label == table)

        print(tabulate(query.dicts(), headers='keys' if header else '', tablefmt=mode))


def exec_tables(**kwargs):
    if kwargs['list']:
        print('\n'.join(TABLES.keys()))
    elif kwargs['modes']:
        print('\n'.join(tabulate_formats))

def exec_fs(**kwargs):

    if type(kwargs['update']) == list and  len(kwargs['update']) == 0:
        kwargs['update'] = ['all']
    kwargs = {k : kwargs[k] for k in kwargs if kwargs[k] }
    if len(kwargs) != 1:
        raise KeyError("can execute only one subcommand at once")
    cmd = list(kwargs.keys())[0]
    arg = kwargs[cmd]

    if cmd == 'list':
        roots = (file.path for file in Node.roots())
        print('\n'.join(roots))

    elif cmd in ('add', 'remove', 'update'):
        indexer = FileIndexer()
        indexer.execute(cmd, arg)

EXEC = {
    "search": exec_search,
    "show": exec_show,
    "tables": exec_tables,
    "fs": exec_fs
}

def execute_command(args: dict) -> None:

    cmd = args.pop('command')
    func = EXEC[cmd]
    func(**args)
