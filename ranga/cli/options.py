"""
cli option types and actions
"""

import argparse

class ActionTemplate(argparse.Action):
    "an example of custom argparse action functor, shall be removed on implementation stage"
    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, values)

ARG_ACTIONS = {
    "ActionTemplate": ActionTemplate
}

ARG_TYPES = {
    'int': int,
    'float': float,
    'str': str,
    'bool': bool
}
