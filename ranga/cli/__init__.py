"""
Command Line Interface
"""

from .parser import build_argparser
ARGPARSER = build_argparser()
