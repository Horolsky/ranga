"""
cli argparser initialization
"""

import argparse
from ranga.util.fs import dirpath, join_path, load_dict
from .options import ARG_ACTIONS, ARG_TYPES

SCRIPT_DIR = dirpath(__file__)
SCHEMA_FILE = join_path(SCRIPT_DIR, "schema.yaml")

APP_INFO = {
    "prog": "ranga",
    "description": "knowledge base manager",
    "usage": None,
    "epilog": None
}

def build_argparser() -> argparse.ArgumentParser:
    "create ArgumentParser instance from options, defined in schema.yaml"

    schema = load_dict(SCHEMA_FILE)
    main_parser = argparse.ArgumentParser(**APP_INFO)
    subparsers = main_parser.add_subparsers(dest='command', help='command help')

    for cmd in schema:
        cmd_parser = subparsers.add_parser(cmd, help=schema[cmd].get('help', ''))
        if schema[cmd].get('mutually-exclusive', False):
            cmd_parser = cmd_parser.add_mutually_exclusive_group()

        for option in schema[cmd].get('options', []):
            invocation = option.pop('invocation')
            if 'type' in option:
                option['type'] = ARG_TYPES[option['type']]
            if 'action' in option:
                if option['action'] in ARG_ACTIONS:
                    option['action'] = ARG_ACTIONS[option['action']]
            cmd_parser.add_argument(*invocation, **option)

    return main_parser
