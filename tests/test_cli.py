"""
test ranga CLI module
"""

from ranga.cli.parser import build_argparser

def test_build_argparser():
    try:
        build_argparser()
    except Exception as e:
        assert False, f"unknown error: {e.name}:{e.msg}, {e.path}, {e.args}"
