"""
test ranga imports
"""

def test_imports():
    try:
        import ranga
        import ranga.app
        import ranga.cli
        import ranga.db
        import ranga.util
    except ImportError as e:
        assert False, f"{e.name}:{e.msg}, {e.path}, {e.args}"
    except Exception as e:
        assert False, f"unknown error: {e.msg}"
