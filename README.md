# RANGA PROJECT

RANGA: knowledge base manager

**Table Of Contents**
 - [FEATURES](#features)
 - [INSTALLATION](#installation)
 - [USAGE](#usage)
   - [Data viewing](#data-viewing)
   - [File indexation](#file-indexation)
   - [Metadata manipulation](#metadata-manipulation)
 - [VERIFICATION](#verification)
 - [DEVS ROADMAP](#devs-roadmap)

## FEATURES

- local file system metadata harvesting
- user-defined categorization
- flexible search queries
- data duplication and version control
- bulk file operations
- cloud storage services integration
- metadata export

## INSTALLATION

* run `pip3 install -e .`
* set the application data directory by adding the `RANGA_APPDIR` variable to the env. If not set, the application will use `<HOME>/.ranga` by default.

## USAGE

### Data viewing

Commands:

* `ranga tables`: database tables information.
    * `-l, --list`: list table invocation.
    * `-m, --modes`: list supported table output formats.
* `ranga show <table>`: show table from database.
* `ranga search`: search metadata by keyword.
    * `-k, --keywords`: keyword value. To search for a partial match, use SQL wildcard syntax.
    * `-c, --category`: specific categories by which to search (filename, path, <metadata key>).

View options for `show` and `search` commands:

* `-m, --mode`: output table format.
* `--headless`: do not render table headers.


### File indexation

Commands:
 * `ranga fs`: local file system operations.
    * `-l, --list`: list root dirs in the index.
    * `-a, --add <dir>`: add directory to the index.
    * `-u, --update <dir>`: update file records in the database.
    * `--remove <dir>`: remove a directory from the index.

### Metadata manipulation
Work in progress...

## VERIFICATION

To test the application locally run `tox` command in the application folder.

GitLab CI: [latest unit tests and coverage report](https://gitlab.com/Horolsky/ranga/-/jobs/artifacts/serverless/browse?job=unit-test-job)

## DEVS ROADMAP
the current prototype design description: [prototype](PROTOTYPE.md)
