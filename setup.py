#!/usr/bin/env python3

from setuptools import setup

tests_require = [
    'pytest>=7.1',
    'pytest-cov>=3',
]
extras_require = {
    'test': tests_require,
}

setup(
    name='ranga',
    version="0.0.1",
    license='',
    description='knowledge base manager',
    long_description='',
    packages=['ranga', 'ranga.app', 'ranga.cli', 'ranga.util'],
    include_package_data=True,
    author="Oleksandr Khorolskyi",
    author_email="oleksandr.khorolskyi@outlook.com",
    url='https://gitlab.com/Horolsky/ranga',
    download_url='',
    zip_safe=False,
    platforms='any',
    install_requires=[
        'peewee>=3.7.0',
        'python_magic>=0.4.27',
        'PyYAML>=5.1',
        'tabulate>=0.8.10'
    ],
    tests_require=tests_require,
    extras_require=extras_require,
    entry_points={
        'console_scripts': [
            'ranga = ranga.__main__:main'
        ]
    }
)
