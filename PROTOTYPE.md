# RANGA: knowledge base manager

**Table Of Contents**
  - [UI Strategy](#ui-strategy)
  - [CLI design](#cli-design)
  - [Application CLI sequence](#application-cli-sequence)
  - [DB schema](#db-schema)
  - [demo files](#demo-files)

## UI Strategy
- CLI as the main front-end
- lightweight GUI with table and graph views

## CLI design

Auto-generation source: [ranga/cli/schema.yaml](ranga/cli/schema.yaml)

## Application CLI sequence

```plantuml
control Client
control Application
database Database as DB
collections Filesystem as FS

note over Client: show / search / tables
Client -> Application: command
Application -> DB: incapsulated sql query
Application <-- DB: raw response
Client <-- Application: formatted response

note over Client: fs add / update
Client -> Application: command
Application -> DB : request root dirs
Application <-- DB
Application -> FS : poll filesystem
Application <-- FS
Application -> DB : update modified

note over Client: fs remove
Client -> Application: command
Application -> DB: request
DB -> DB : recursively remove data
```

## DB schema

db schema is designed as a polymorphic (i. e. attribute agnostic) 3-dimensional data registry.  
The only hardcoded attributes are the fields **path** and **modified** in the **Files** table.  
Metadata is stored in 3 tables:
 - MetaKeys:  stores metadata keys as objects
 - MetaData:  metadata key-to-value mapping
 - MetaMap:   file-to-metadata mapping  

Advantages over the hardcoded table:
 - correct handling of list records (e. g. for keys like **author**, **genre**, **category**), which is not supported in SQLite     
 - flexible structure, db module is independent from metadata attributes

```plantuml
@startuml ranga-erd
left to right direction

legend top left
    |<#DarkGray> **ENTITY** |<#DarkGray> **DESCRIPTION**            |
    |<#red> **T**           |<#red> **Tables**                      |
    | Node                  | artifacts: directories, files, etc    |
    | Key                   | Metadata attribute key                |
    | Attribute             | Metadata attribute value              |
    | Category              | Metadata artifact-attribute relation  |
    |<#blue> **V**          |<#blue> **Views**                      |
    | AttributeView         | combined attribute key+value view     |
    | MetaData              | full categories view                  |
    |<#Magenta> **E**       |<#Magenta> **Triggers**                |
    | InsertNode            | Node setter                           |
    | InsertMetaData        | MetaData setter                       |
endlegend

!define pk <b><color:#b8861b><&key></color> id: int</b>
!define fk(x) <b><color:#aaaaaa><&key></color> x</b>
!define column(x) <color:#efefef><&media-record></color> x
!define table(x) entity x << (T, red) >>
!define view(x) entity x << (V, blue) >>
!define trigger(x) entity x << (E, Magenta) >>

table("Node") {
    pk
    column(path): str
    fk(parent): Node
}

table(Key) {
    pk
    column(label): str
    column(descr): str
}

table(Attribute) {
    pk
    fk(key): Key
    column(value): str
}

table(Category) {

    fk(node): Node
    fk(attribute): Attribute
}

"Node" }|--|| "Node"
"Attribute" }|--|| "Key"
"Category" }|--|| "Node"
"Category" }|--|| "Attribute"

view(AttributeView) {
    fk(key): Key
    fk(attribute): Attribute
    column(label): Key.label
    column(descr): Key.descr
    column(value): Attribute.value
    ---
    join: Key, Attribute
}

view(MetaData) {
    fk(node): Node
    fk(key): Key
    fk(attribute): Attribute

    column(path): Node.path
    column(parent): Node.parent
    column(label): AttributeView.label
    column(descr): AttributeView.descr
    column(value): AttributeView.value

    ---
    join: AttributeView, Category, Node
}

"AttributeView" }|--|| "Key"
"AttributeView" }|--|| "Attribute"

"MetaData" }|--|| "Node"
"MetaData" }|--|| "Key"
"MetaData" }|--|| "Attribute"


trigger(InsertNode) {
    If 'parent' field is given as a path,
    substitute it with the corresponding id
}

trigger(InsertMetaData) {
    Instead of INSERT action on MetaData,
    redirect the data to corresponding tables
}

"InsertNode" --> "Node"
"InsertMetaData" --> "MetaData"
@enduml

```

## demo files

To download media assets for testing, run [scripts/get_media.sh](scripts/get_media.sh)  
You can add new asset urls to [scripts/links.txt](scripts/links.txt)  
